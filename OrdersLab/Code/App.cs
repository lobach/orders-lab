﻿namespace OrdersLab.Code;

public class App
{
    private readonly TaskData _taskData;

    public App(TaskData taskData) =>
        _taskData = taskData;

    public App Run(IBranching branching, ILimitSolver downSolver, ILimitSolver topSolver)
    {
        var nodes = new List<Node>{new()};
        while (true)
        {
            //Stop
            if (nodes.Count == 1 && nodes[0].Depth == _taskData.Size)
                break;

            //Branch
            var vs = branching.Branch(nodes);
            nodes.Clear();
            foreach (var v in vs)
            {
                var b = v.GetB(_taskData.Size);

                foreach (var i in b)
                    nodes.Add(new Node(v)
                        .Add(i)
                        .Calc(_taskData));
            }

            //Cutout
            var toRemove = new List<Node>();
            foreach (var v1 in nodes)
            foreach (var v2 in nodes)
            {
                if (v1 == v2) continue;

                if (topSolver.Solve(_taskData, v1) < downSolver.Solve(_taskData, v2))
                    toRemove.Add(v2);
            }

            foreach (var n in toRemove)
                nodes.Remove(n);
        }

        var solve = nodes[0];
        Console.WriteLine($"Delayed: {solve.Delayed}");
        foreach (var i in solve.Path)
            Console.Write($"{i} ");

        Console.WriteLine();

        return this;
    }
}