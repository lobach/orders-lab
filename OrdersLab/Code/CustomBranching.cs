namespace OrdersLab.Code;

public class CustomBranching : IBranching
{
    private readonly TaskData _taskData;
    private readonly ILimitSolver _downSolver;
    private readonly ILimitSolver _topSolver;

    public CustomBranching(TaskData taskData, ILimitSolver downSolver, ILimitSolver topSolver)
    {
        _taskData = taskData;
        _downSolver = downSolver;
        _topSolver = topSolver;
    }

    public List<Node> Branch(List<Node> node)
    {
        var minValue = int.MaxValue;
        var minIndex = -1;
        for (var i = 0; i < node.Count; i++)
        {
            var solveD = _downSolver.Solve(_taskData, node[i]);
            var solveT = _topSolver.Solve(_taskData, node[i]);
            var solve = (solveD + solveT) / 2;
            if (solve <= minValue)
            {
                minIndex = i;
                minValue = solve;
            }
        }

        return new List<Node> {node[minIndex]};
    }
}