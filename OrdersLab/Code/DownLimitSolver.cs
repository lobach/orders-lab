namespace OrdersLab.Code;

public class DownLimitSolver : ILimitSolver
{
    public int Solve(TaskData data, Node node)
    {
        var b = node.GetB(data.Size);
        var newDelayed = 0;
        var last = node.Path.LastOrDefault();
        foreach (var i in b)
            if (data.DirectTimes[i] <= node.Time + data.Times[last, i])
                newDelayed++;

        return node.Delayed + newDelayed;
    }
}