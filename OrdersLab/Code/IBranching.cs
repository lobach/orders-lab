﻿namespace OrdersLab.Code;

public interface IBranching
{
    public List<Node> Branch(List<Node> node);
}