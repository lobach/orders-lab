﻿namespace OrdersLab.Code;

public interface ILimitSolver
{
    public int Solve(TaskData data, Node node);
}