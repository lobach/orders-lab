﻿namespace OrdersLab.Code;

public class Node
{
    public int Depth => Path.Count;
    public List<int> Path { get; }
    public int Time { get; set; }
    public int Delayed { get; set; }

    public Node() =>
        Path = new List<int>{0};

    public Node(Node node) =>
        Path = new List<int>(node.Path);

    public Node Add(int path)
    {
        Path.Add(path);
        return this;
    }

    public List<int> GetB(int size)
    {
        var b = new List<int>();
        for (var i = 0; i < size; i++)
            if (!Path.Contains(i))
                b.Add(i);

        return b;
    }

    public Node Calc(TaskData data)
    {
        var time = 0;
        var prev = 0;
        var delayed = 0;
        foreach (var n in Path)
        {
            time += data.Times[prev, n];
            if (time > data.DirectTimes[n])
                delayed++;

            prev = n;
        }

        Time = time;
        Delayed = delayed;

        return this;
    }
}