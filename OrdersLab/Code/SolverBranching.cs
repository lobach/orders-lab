namespace OrdersLab.Code;

public class SolverBranching : IBranching
{
    private readonly TaskData _taskData;
    private readonly ILimitSolver _solver;

    public SolverBranching(TaskData taskData, ILimitSolver solver)
    {
        _taskData = taskData;
        _solver = solver;
    }

    public List<Node> Branch(List<Node> node)
    {
        var minValue = int.MaxValue;
        var minIndex = -1;
        for (var i = 0; i < node.Count; i++)
        {
            var solve = _solver.Solve(_taskData, node[i]);
            if (solve <= minValue)
            {
                minIndex = i;
                minValue = solve;
            }
        }

        return new List<Node> {node[minIndex]};
    }
}