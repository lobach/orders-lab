﻿namespace OrdersLab.Code;

public class TaskData
{
    public int Size { get; init; }
    public int[] DirectTimes { get; init; }
    public int[,] Times { get; init; }
}