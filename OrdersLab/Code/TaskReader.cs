﻿namespace OrdersLab.Code;

public static class TaskReader
{
    public static TaskData Read(string fileName)
    {
        var lines = File.ReadAllLines(fileName);
        var size = lines[0].ToInt() + 1;
        var directTimes = ("0 " + lines[1])
            .Replace('\t', ' ')
            .Split(' ')
            .Where(s => !string.IsNullOrEmpty(s))
            .Select(s => s.ToInt())
            .ToArray();
        var times = new int[size, size];
        for (var i = 2; i < size + 2; i++)
        {
            var tmp = lines[i]
                .Replace('\t', ' ')
                .Split(' ')
                .Where(s => !string.IsNullOrEmpty(s))
                .Select(s => s.ToInt())
                .ToArray();
            for (var j = 0; j < size; j++)
                times[i - 2, j] = tmp[j];
        }

        return new TaskData
        {
            Size = size,
            DirectTimes = directTimes,
            Times = times
        };
    }
}