namespace OrdersLab.Code;

public class TopLimitSolver : ILimitSolver
{
    public int Solve(TaskData data, Node node)
    {
        var newNode = new Node(node);
        while (true)
        {
            if (newNode.Depth == data.Size) break;

            var b = newNode.GetB(data.Size);
            var times = new List<Pair>();
            foreach (var i in b) 
                times.Add(new Pair(i, newNode.Time + data.DirectTimes[i]));

            var min = new Pair(int.MaxValue, int.MaxValue);
            foreach(var i in times)
                if (i.Time <= min.Time)
                    min = i;

            newNode.Add(min.Index).Calc(data);
        }

        return newNode.Delayed;
    }
    
    private readonly struct Pair
    {
        public readonly int Index;
        public readonly int Time;

        public Pair(int index, int time)
        {
            Index = index;
            Time = time;
        }
    }
}