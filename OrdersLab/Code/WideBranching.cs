namespace OrdersLab.Code;

public class WideBranching : IBranching
{
    public List<Node> Branch(List<Node> node) => 
        new() {node[Random.Shared.Next(0, node.Count)]};
}