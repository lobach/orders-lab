﻿using OrdersLab.Code;

namespace OrdersLab;

public static class Program
{
    public static void Main(string[] args)
    {
        var path = "./Data";
        var dir = new DirectoryInfo(path!);
        foreach(var file in dir.GetFiles())
        {
            Console.WriteLine("=======================");
            Console.WriteLine($"FILE: {file.Name}");
            var taskData = TaskReader.Read(file.FullName);
            var app = new App(taskData);

            var down = new DownLimitSolver();
            var top = new TopLimitSolver();

            Console.WriteLine("STOCK");
            IBranching branching = new SolverBranching(taskData, top);
            app.Run(branching, down, top);

            Console.WriteLine("CUSTOM");
            branching = new CustomBranching(taskData, down, top);
            app.Run(branching, down, top);
            Console.WriteLine("=======================");
        }
    }
}